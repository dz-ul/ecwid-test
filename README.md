# README #
**Описание тестовой задачи:**

**При входе на тестовый сервер нужно нажать кнопку(ссылку) "Дополнительно" - небезопасное соединение**

*1. Делаем Javascript-ом + JQuery. На странице оплаты заказа есть поле которое называется Order Comments. Оно одно и второго такого поля мы добавить туда силами Эквид не можем. Значит давайте добавим кастовое. А именно напишем скрипт, который под дефолтным полем создаст его копию, пусть оно будет называться “Specify needed delivery date here:” В самом поле должна применяться маска даты в формате MM/DD/YYYY
Покупатель вбивает туда дату и проходит оплачивать/размещать заказ. Поле должно быть обязательным. То есть покупатель не сможет пройти на чикаут, пока не заполнит это поле. 
В бэкенде наш скрипт должен просто приаттачить вот эту дату, которую он вбил к родному полю Orders Comments, чтобы мы могли увидеть эту дату в админке Эквид в заказах.*

Для применения маски даты в формате MM/DD/YYYY был использован плагин:
jQuery Masked Input Plugin
Copyright (c) 2007 - 2015 Josh Bush (digitalbush.com)
Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license)
Version: 1.4.1

*2. Делаем с помощью приложения (вот здесь нам как раз понадобится API). Разместите в магазине несколько заказов, например, на $1000. Далее переведите 8 заказов из 10 в статус Paid (оплачен). Это можно сделать в Панели управления Эквида > Sales > Orders
Далее нужно написать такое приложение, которое будет обращаться по API в магазин, вытягивать всю информацию о заказах со статусом “оплачен“ и считать общую сумму этих заказов. Далее приложение должно создать какой-нибудь отдельный блок на главной странице магазина и выводить там полученное значение. Например “Мы уже продали на $800”.*

## **Исполняемые файлы находятся по адресу:** ##
/shipping/custom.js

## **Исполняемые файлы WordPress:** ##
/wp-admin

/wp-content

/wp-includes

### Для выполнения задачи необходимо выполнить: ###

* Регистрация на сайте https://my.ecwid.com
* Настроить сайт на свой домен и указать нужные параметры

### Настроить сайт для выполнения Ecwid API ###
Для регистрации сайта https://172.22.22.78 на выполнение Ecwid API нужно ввести следующие шаги (https://172.22.22.78 данный URL указан для примера)

* Your email
**yw_80@mail.ru**
* Briefly describe your application's function(s) and benefit(s)
**тестовое задание по Эквид**
* App name
**test**
* Is your app for some specific store(s), or are you going to submit it to the Ecwid App Market for all merchants to use?
**Ecwid App Market**
* App author/developer
**developer**
* App website
**https://172.22.22.78**
* Developer website
**https://172.22.22.78**
* App help URL
**https://172.22.22.78/help.html**
* How your app will charge users?
**Ecwid billing**
* Is it a web application?
**It's a web application**
* Does your app change the user storefront appearance or add custom JavaScript to any store pages?
**Yes**

### **Storefront customizations** ###

* HTTPS link to your custom CSS file
**https://172.22.22.78/wp-admin/test/custom.css**

* HTTPS link to your custom JS file
**https://172.22.22.78/shipping/custom.js**

* Do you want to add your app interface to the Ecwid Control Panel?
**Yes**

* App interface settings inside Ecwid Control Panel
Control Panel section that your app will be added to
**Sales**

* Application's tab title in the Control Panel
**TestSales**

* URL of your app that will be loaded in the Control Panel
**https://172.22.22.78/wp-admin/test/iframe.html**

* Will your app calculate and display additional shipping methods?
**Yes**

* Please specify request URL for your application.
**https://172.22.22.78/shipping/calculate.php**

* oAuth return URL
**https://172.22.22.78/shipping/auth.php**

* Will you need to utilize webhooks to your application endpoint?
**Yes**

* Please specify an HTTPS URL for your webhooks endpoint
**https://172.22.22.78/shipping/webhooks.js**

* Please select which event types you will want to be notified on with webhooks
**unfinished_order.created, unfinished_order.updated, unfinished_order.deleted, order.created, order.updated, order.deleted, product.created, product.updated, product.deleted**

* List of permissions your app will ask by default
**read_store_profile, read_catalog, read_orders, read_customers, read_discount_coupons, update_store_profile, update_catalog, update_orders, update_customers, update_discount_coupons, create_catalog, create_orders, create_customers, create_discount_coupons**

* By selecting "Yes" below, I confirm that I have read and accept the terms and conditions of the Ecwid App Market and API
**Yes**


### После успешной регистрации мы получаем на нашу почту ###

* client_id: test-task-yw-80
* client_secret: wCsVxjo2sQTcyA9esRBAdbRkfXATapeR

### Далее для выполненя наших кастомных файлов нужно выполнить следующее  ###
Выполняеться в браузере в строке ввода адреса

* https://my.ecwid.com/api/oauth/authorize?client_id=test-task-yw-80&redirect_uri=https://172.22.22.78/shipping/auth.php&response_type=code&scope=read_store_profile+read_catalog+read_orders+read_customers+read_discount_coupons+update_store_profile+update_catalog+update_orders+update_customers+update_discount_coupons+create_catalog+create_orders+create_customers+create_discount_coupons+add_to_cp+add_shipping_method+customize_storefront
* https://172.22.22.78/shipping/auth.php?code=PrmqC36DCyJeYpPUyp6znwtwvQwTaKM5
* https://my.ecwid.com/api/oauth/token?client_id=test-task-yw-80&client_secret=wCsVxjo2sQTcyA9esRBAdbRkfXATapeR&code=PrmqC36DCyJeYpPUyp6znwtwvQwTaKM5&redirect_uri=http://172.22.22.242:8000/shipping/auth.php&grant_type=authorization_code

После этого мы получаем:

* **access_token:** "b195yBnMXuA5rYJkUaBigYJn9HdY38t3"

* **token_type:** "Bearer"

* **scope**: "read_store_profile update_store_profile create_orders update_orders read_orders create_catalog update_catalog read_catalog add_to_cp update_discount_coupons create_discount_coupons read_discount_coupons create_customers update_customers read_customers customize_storefront add_shipping_method"

* **store_id:** 9248020